# build
FROM node:16-slim as builder

WORKDIR /usr/src/app

ARG API_KEY
ENV API_KEY=${API_KEY}

COPY package.json ./package.json
COPY package-lock.json ./package-lock.json

RUN npm install --silent
COPY . ./

RUN npm run build --silent


# production
FROM node:16-slim

WORKDIR /usr/src/app

ARG API_KEY
ENV API_KEY=${API_KEY}

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY --from=builder /usr/src/app/build ./build
COPY --from=builder /usr/src/app/server.js ./server.js
COPY --from=builder /usr/src/app/LICENSE.txt ./LICENSE.txt
COPY --from=builder /usr/src/app/README.md  ./README.md

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 react
RUN chown -R react:nodejs /usr/src/app
USER react

EXPOSE 5200

CMD ["node", "./server.js"]
