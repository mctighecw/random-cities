# README

This is a React app that selects a random city via the _Geobytes Get City Details API_. It was meant to be a simple and fun project.

It is the first time that I have used _CSS modules_ for styling. LESS classes are hashed, while CSS is applied normally.

## App Information

App Name: random-cities

Created: May 2018; updated 2018, 2021, 2023

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/random-cities)

Production: [Link](https://random-cities.mctighecw.site)

License: MIT License

## Tech Stack

- React
- React Router
- LESS
- CSS Modules
- Geobytes Get City Details API
- Node.js

## Production

1. Build

```
$ docker build --build-arg API_KEY=<GEOBYTES_API_KEY> -t random-cities .
```

2. Run

```
$ docker run --rm -p 80:5200 --name random-cities random-cities
```

Last updated: 2024-12-17
