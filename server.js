const path = require("path");
const express = require("express");
const requests = require("requests");
const morgan = require("morgan");

const app = express();
const port = process.env.PORT || 5200;

app.use(morgan("common"));
app.use(express.static(__dirname + "/build"));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.get("/geodata", (req, res) => {
  const code = req.query["code"];
  const seconds = new Date().getTime() / 1000;
  const url = `https://secure.geobytes.com/GetCityDetails?key=${process.env.API_KEY}&fqcn=${code}&_=${seconds}`;

  requests(url, { timeout: 10000 })
    .on("data", (data) => {
      res.status(200).json(data);
    })
    .on("end", (err) => {
      if (err) {
        console.error(err);
        res.status(503).json({ status: "An error has occurred" });
      }
    });
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "/build", "index.html"));
});

app.listen(port, () => {
  console.log(`Node server is running on port ${port}`);
});

process.on("SIGINT", () => {
  console.info("Stopping Node server");
  process.exit(0);
});
