import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import axios from 'axios';

import Home from 'Pages/Home/Home';
import City from 'Pages/City/City';
import Info from 'Pages/Info/Info';
import Wiki from 'Pages/Wiki/Wiki';

import cities from './data/cities.json';
import './styles/styles.css';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null
    };
  }

  getRandomCity = () => {
    const index = Math.floor(Math.random() * cities.length);
    return cities[index];
  }

  getCityData = () => {
    const city = this.getRandomCity();
    const prefix =
      process.env.NODE_ENV === 'development' ? 'http://localhost:5200' : '';
    const url = `${prefix}/geodata?code=${city}`;

    axios({
      method: 'GET',
      url,
    })
      .then((res) => {
        const data = JSON.parse(res.data);
        this.setState({ data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  componentWillMount() {
    this.getCityData();
  }

  render() {
    const { data } = this.state;

    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/city" render={() => <City data={data} />} />
        <Route path="/info" render={() => <Info data={data} />} />
        <Route path="/wiki" render={() => <Wiki data={data} />} />
        <Route render={() => <Redirect to="/" />} />
      </Switch>
    )
  }
}

export default App;
