import React from 'react';
import './styles.less';

const Button = ({ style, label, onClickMethod }) =>  (
  <button styleName={style} onClick={onClickMethod}>
    <span>{label}</span>
  </button>
);

export default Button;
