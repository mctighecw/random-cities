import React from 'react';
import './styles.less';

const Footer = () => (
  <footer styleName="footer">
    <div styleName="text">Copyright © 2018, Christian McTighe. Coded by Hand.</div>
  </footer>
);

export default Footer;
