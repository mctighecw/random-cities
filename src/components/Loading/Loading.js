import React from 'react';
import spinner from 'Assets/loading-spinner.svg';
import './styles.less';

const Loading = ({ label }) => (
  <div styleName="box">
    <img src={spinner} alt="" />
    <div styleName="text">{label}...</div>
  </div>
)

export default Loading;
