import React from 'react';
import { withRouter } from 'react-router-dom';
import skyline from 'Assets/skyline.svg';
import './styles.less';

const Top = ({ title, history }) => {

  const handleClickNav = (href) => {
    if (href !== history.location.pathname) {
      history.push(href);
    }
  }

  return (
    <div>
      <header styleName="header">
        <img src={skyline} styleName="skyline" alt=""  />
        <h1 styleName="title">{title}</h1>
      </header>
      <div styleName="nav">
        <div onClick={() => handleClickNav("/")} styleName="nav-item" style={{ fontWeight: history.location.pathname === "/" ? 600 : 300 }}>Home</div> |
        <div onClick={() => handleClickNav("/city")} styleName="nav-item" style={{ fontWeight: history.location.pathname === "/city" ? 600 : 300 }}>City</div> |
        <div onClick={() => handleClickNav("/info")} styleName="nav-item" style={{ fontWeight: history.location.pathname === "/info" ? 600 : 300 }}>Info</div> |
        <div onClick={() => handleClickNav("/wiki")} styleName="nav-item" style={{ fontWeight: history.location.pathname === "/wiki" ? 600 : 300 }}>Wiki</div>
      </div>
    </div>
  )
}

export default withRouter(Top);
