import React from 'react';
import { withRouter } from 'react-router-dom';
import Top from 'Components/Top/Top';
import Footer from 'Components/Footer/Footer';
import Loading from 'Components/Loading/Loading';
import Button from 'Components/Button/Button';
import './styles.less';

const City = ({ history, data }) => (
  <div className="container">
    <div className="content">
      <Top title="Your City" />

      {data !== null ?
        <div>
          <div styleName="your-city">
            <span>Your random city is</span><br />
            <span styleName="city-name">{data.geobytescity}</span>
          </div>

          <div styleName="center-button">
            <Button
              label="Info"
              style="button"
              onClickMethod={() => history.push("/info")}
            />
          </div>

          <div styleName="new-city">
            (Just hit reload on your browser to get a new city)
          </div>
        </div>
        :
        <Loading label="Finding a city..." />
      }
    </div>

    <Footer className="footer" />
  </div>
);

export default withRouter(City);
