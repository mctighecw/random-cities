import React from 'react';
import { withRouter } from 'react-router-dom';

import Top from 'Components/Top/Top';
import Footer from 'Components/Footer/Footer';
import Button from 'Components/Button/Button';

import newYork from 'Assets/new-york.jpg';
import './styles.less';

const Home = ({ history }) => (
  <div className="container">
    <div className="content">
      <Top title="Random Cities" />

      <img src={newYork} alt="" styleName="city-pic" />

      <div styleName="description">
        <p>Have you ever wanted to know more about other cities?</p>
        <p>Click below to discover a random city!</p>
      </div>

      <div styleName="center-button">
        <Button
          label="City"
          style="button"
          onClickMethod={() => history.push("/city")}
        />
      </div>
    </div>

    <Footer className="footer" />
  </div>
);

export default withRouter(Home);
