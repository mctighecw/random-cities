import React from 'react';
import Top from 'Components/Top/Top';
import Footer from 'Components/Footer/Footer';
import Loading from 'Components/Loading/Loading';
import './styles.less';

const Info = ({ data }) => {
  const formatPopulation = (pop) => {
    return pop.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <div className="container">
      <div className="content">
        <Top title="Info" />

        {data !== null ?
          <div styleName="data">
            <div styleName="heading">
              GeoBytes City Data
            </div>
            <p>
              City: {data.geobytescity}
            </p>
            <p>
              Country Capital: {data.geobytescapital}
            </p>
            <p>
              Country: {data.geobytescountry} ({data.geobytestitle})
            </p>
            <p>
              Region: {data.geobytesmapreference}
            </p>
            <p>
              Nationality: {data.geobytesnationalitysingular}
            </p>
            <p>
              Country Population: {formatPopulation(data.geobytespopulation)}
            </p>
            <p>
              Currency: {data.geobytescurrency} ({data.geobytescurrencycode})
            </p>
            <p>
              Internet Suffix: {data.geobytesinternet}
            </p>
            <p>
              Latitude: {data.geobyteslatitude}
            </p>
            <p>
              Longitude: {data.geobyteslongitude}
            </p>
            <p>
              Timezone: GMT {data.geobytestimezone}
            </p>
          </div>
          :
          <Loading label="Getting city info..." />
        }
      </div>

      <Footer className="footer" />
    </div>
  )
}

export default Info;
