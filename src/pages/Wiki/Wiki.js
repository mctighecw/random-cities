import React from 'react';

import Top from 'Components/Top/Top';
import Footer from 'Components/Footer/Footer';
import Loading from 'Components/Loading/Loading';
import Button from 'Components/Button/Button';
import './styles.less';

const Wiki = ({ data }) => (
  <div className="container">
    <div className="content">
      <Top title="Wiki" />


      {data !== null ?
        <div>
          <div styleName="description">
            Find out more about {data.geobytescity} on the Wikipedia.
          </div>
          <div styleName="center-button">
            <Button
              label="Wikipedia"
              style="button"
              onClickMethod={() => window.open(`http://en.wikipedia.org/wiki/${data.geobytescity}`)}
            />
          </div>
        </div>
        :
        <Loading label="Getting city..." />
      }
    </div>

    <Footer className="footer" />
  </div>
)

export default Wiki;
